/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

//! Types to parse class-related syntax.

use crate::concrete::{
    child_span, ErrorType, Kind, LineTerminator, Node, Parse, ParseContext, ParseResult,
};
use gdscript_lexer::token::{Keyword, Punctuation, TokenContent, TokenKind};

/// An "inner" class.
pub struct Class;

impl Parse for Class {
    fn accept(_parser: &ParseContext<impl AsRef<str>>, tok: TokenContent) -> bool {
        match tok {
            TokenContent::Keyword(kwd) => match kwd {
                Keyword::Class => true,
                _ => false,
            },
            _ => false,
        }
    }

    fn parse(_parser: &mut ParseContext<impl AsRef<str>>) -> ParseResult {
        unimplemented!()
    }
}

/// A class item.
///
/// All current class-items are:
///  - Member variables
///  - functions (member and static)
///  - inner types (such as enums and classes)
///  - "tool", for editor-runnable classes.
///  - "extends", to specify an object-inheritance relationship
///  - "class_name", to create a unique name identifying the script
///  - documentation comments in the form of strings.
pub struct Item;

impl Parse for Item {
    fn accept(parser: &ParseContext<impl AsRef<str>>, tok: TokenContent) -> bool {
        if !parser.is_correct_indentation() {
            return false;
        }

        if parser.accept::<Member>(tok) {
            return true;
        }

        if parser.accept::<Function>(tok) {
            return true;
        }

        if parser.accept::<InnerType>(tok) {
            return true;
        }

        match tok {
            TokenContent::Keyword(Keyword::Tool) => true,
            TokenContent::Keyword(Keyword::Extends) => true,
            TokenContent::Keyword(Keyword::ClassName) => true,
            TokenContent::String(_) => true,
            _ => false,
        }
    }

    fn parse(parser: &mut ParseContext<impl AsRef<str>>) -> ParseResult {
        let tok = parser.peek();

        if parser.accept::<Member>(tok.content) {
            Ok(parser.parse::<Member>())
        } else if parser.accept::<Function>(tok.content) {
            Ok(parser.parse::<Function>())
        } else if parser.accept::<InnerType>(tok.content) {
            Ok(parser.parse::<InnerType>())
        } else {
            match tok.content {
                TokenContent::Keyword(Keyword::Tool) => {
                    let mut tool = parser.eat_token(Kind::KwTool);
                    let terminator = parser.parse::<LineTerminator>();

                    tool.add_child(terminator);

                    Ok(tool)
                }
                TokenContent::Keyword(Keyword::Extends) => {
                    let mut extends = parser.eat_token(Kind::KwExtends);

                    let path = parse_try!(parser, ExtendsPath, extends);

                    let newline = parse_try!(parser, LineTerminator, extends);

                    extends.add_child(path);
                    extends.add_child(newline);

                    Ok(extends)
                }
                TokenContent::Keyword(Keyword::ClassName) => {
                    let mut kwd = parser.eat_token(Kind::ClassItemClassName);

                    let name = parse_expect!(parser, TokenKind::Identifier, Kind::Identifier, kwd);

                    kwd.add_child(name);

                    // icon path thingy
                    if parser.is(TokenKind::String) {
                        kwd.add_child(parser.eat_token(Kind::String));
                    }

                    let newline = parse_try!(parser, LineTerminator, kwd);
                    kwd.add_child(newline);

                    Ok(kwd)
                }
                TokenContent::String(_) => {
                    let string = parser.eat_token(Kind::DocComment);

                    let newline = parser.parse::<LineTerminator>();

                    let children = vec![string, newline];

                    Ok(Node {
                        kind: Kind::ClassItemDocComment,
                        span: child_span(&children),
                        children,
                    })
                }
                _ => Err(parser.emit_error(ErrorType::UnexpectedToken)),
            }
        }
    }
}

/// Class member-variable.
pub struct Member;

impl Parse for Member {
    fn accept(_parser: &ParseContext<impl AsRef<str>>, tok: TokenContent) -> bool {
        match tok {
            TokenContent::Keyword(kwd) => match kwd {
                Keyword::OnReady | Keyword::Export | Keyword::Const | Keyword::Var => true,
                _ => false,
            },
            _ => false,
        }
    }

    fn parse(_parser: &mut ParseContext<impl AsRef<str>>) -> ParseResult {
        unimplemented!()
    }
}

/// A static- or member-function in a class.
pub struct Function;

impl Parse for Function {
    fn accept(parser: &ParseContext<impl AsRef<str>>, tok: TokenContent) -> bool {
        if parser.accept::<FunctionAttr>(tok) {
            return true;
        }

        match tok {
            TokenContent::Keyword(Keyword::Func) => true,
            _ => false,
        }
    }

    fn parse(parser: &mut ParseContext<impl AsRef<str>>) -> ParseResult {
        let tok = parser.peek();

        let mut node = parser.empty_node(Kind::Function);

        if parser.accept::<FunctionAttr>(tok.content) {
            let attr = parse_try!(parser, FunctionAttr, node);

            node.add_child(attr);
        }

        let fn_ = parse_expect_tok!(
            parser,
            TokenContent::Keyword(Keyword::Func),
            Kind::KwFunc,
            node
        );
        node.add_child(fn_);

        let name = parse_expect!(parser, TokenKind::Identifier, Kind::Identifier, node);
        node.add_child(name);

        // TODO draw the rest of the owl

        Ok(node)
    }
}

/// Function attributes.
pub struct FunctionAttr;

impl Parse for FunctionAttr {
    fn accept(_parser: &ParseContext<impl AsRef<str>>, tok: TokenContent) -> bool {
        match tok {
            TokenContent::Keyword(kwd) => match kwd {
                Keyword::Static
                | Keyword::Remote
                | Keyword::Sync
                | Keyword::Master
                | Keyword::Puppet
                | Keyword::Slave
                | Keyword::RemoteSync
                | Keyword::MasterSync
                | Keyword::PuppetSync => true,
                _ => false,
            },
            _ => false,
        }
    }

    fn parse(parser: &mut ParseContext<impl AsRef<str>>) -> ParseResult {
        let tok = parser.peek();

        let mut node = parser.empty_node(Kind::FuncAttribute);

        let attr = match tok.content {
            TokenContent::Keyword(a) => match a {
                Keyword::Static => parser.eat_token(Kind::KwStatic),
                Keyword::Remote => parser.eat_token(Kind::KwRemote),
                Keyword::Sync => parser.eat_token(Kind::KwSync),
                Keyword::Master => parser.eat_token(Kind::KwMaster),
                Keyword::Puppet => parser.eat_token(Kind::KwPuppet),
                Keyword::Slave => parser.eat_token(Kind::KwSlave),
                Keyword::RemoteSync => parser.eat_token(Kind::KwRemoteSync),
                Keyword::MasterSync => parser.eat_token(Kind::KwMasterSync),
                Keyword::PuppetSync => parser.eat_token(Kind::KwPuppetSync),
                _ => parser.emit_error(ErrorType::UnexpectedToken),
            },
            _ => parser.emit_error(ErrorType::UnexpectedToken),
        };

        node.add_child(attr);

        if node.children[0].is_error() {
            Err(node)
        } else {
            Ok(node)
        }
    }
}

/// An inner type of a class, such as enums or other classes.
pub struct InnerType;

impl Parse for InnerType {
    fn accept(_parser: &ParseContext<impl AsRef<str>>, tok: TokenContent) -> bool {
        match tok {
            TokenContent::Keyword(kwd) => match kwd {
                Keyword::Class | Keyword::Enum => true,
                _ => false,
            },
            _ => false,
        }
    }

    fn parse(_parser: &mut ParseContext<impl AsRef<str>>) -> ParseResult {
        unimplemented!()
    }
}

/// The path describing the super-class.
struct ExtendsPath;

impl Parse for ExtendsPath {
    fn accept(_parser: &ParseContext<impl AsRef<str>>, tok: TokenContent) -> bool {
        if tok.is_kind(TokenKind::String) {
            true
        } else if tok.is_kind(TokenKind::Identifier) {
            true
        } else {
            false
        }
    }

    fn parse(parser: &mut ParseContext<impl AsRef<str>>) -> ParseResult {
        let tok = parser.peek();

        let initial_path = match tok.content.get_kind() {
            TokenKind::Identifier => {
                let ident = parser.eat_token(Kind::Identifier);

                Node {
                    kind: Kind::ExtendsPath,
                    span: ident.span,
                    children: vec![ident],
                }
            }
            TokenKind::String => {
                let string = parser.eat_token(Kind::String);

                Node {
                    kind: Kind::ExtendsPath,
                    span: string.span,
                    children: vec![string],
                }
            }
            _ => {
                return Err(parser.emit_error(ErrorType::UnexpectedToken));
            }
        };

        let mut node = initial_path;

        loop {
            let tok = parser.peek();

            // not a dot? Done!
            if tok.content != TokenContent::Punctuation(Punctuation::Period) {
                return Ok(node);
            }

            let dot = parser.eat_token(Kind::Period);

            let next_atom = parse_expect!(parser, TokenKind::Identifier, Kind::Identifier, node);

            let new_node = Node {
                kind: Kind::ExtendsPath,
                span: (node.span.0, next_atom.span.1),
                children: vec![node, dot, next_atom],
            };

            node = new_node;
        }
    }
}
