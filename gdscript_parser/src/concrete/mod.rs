/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

//! Parsing of tokens into a concrete syntax tree.

/// Skip all tokens until a previous parser can accept it again.
#[macro_export]
macro_rules! parse_recover {
    ($parser:expr, $return:expr) => {
        if $parser.is_follow() {
            // emit error without consuming
            let error = $parser.emit_error($crate::concrete::ErrorType::UnexpectedToken);
            $return.add_child(error);
            Err($return)
        } else {
            while !$parser.is_follow() && !$parser.is_eof() {
                let error = $parser.eat_error($crate::concrete::ErrorType::UnexpectedToken);
                $return.add_child(error);
            }

            if $parser.is_eof() {
                let error = $parser.eat_error($crate::concrete::ErrorType::UnexpectedToken);
                $return.add_child(error);
            }

            Err($return)
        }
    };
}

/// Expect a specific token, use [`parse_recover`] otherwise.
///
/// [`parse_recover`]: ./macro.parse_recover.html
#[macro_export]
macro_rules! parse_expect_tok {
    ($parser:expr, $tok:expr, $kind:expr, $return:expr) => {
        if let Some(res) = $parser.expect_tok($tok, $kind) {
            res
        } else {
            return parse_recover!($parser, $return);
        }
    };
}

/// Expect a token with a certain kind, user [`parse_recover`] otherwise.
#[macro_export]
macro_rules! parse_expect {
    ($parser:expr, $tok:expr, $kind:expr, $return:expr) => {
        if let Some(res) = $parser.expect($tok, $kind) {
            res
        } else {
            return parse_recover!($parser, $return);
        }
    };
}

/// Unwrap a [`ParseResult`] into a [`Node`].
///
/// [`ParseResult`]: ./type.ParseResult.html
/// [`Node`]: ./struct.Node.html
#[macro_export]
macro_rules! parse_unwrap {
    ($result:expr) => {
        match $result {
            Ok(node) => node,
            Err(node) => node,
        }
    };
}

/// Attempt to run a parser. If succeeds, return parsed [`Node`],
/// otherwise use [`parse_recover`] and return.
#[macro_export]
macro_rules! parse_try {
    ($parser:expr, $parse_item:ty, $return:expr) => {
        match <$parse_item as $crate::concrete::Parse>::parse($parser) {
            Ok(res) => res,
            Err(node) => {
                $return.add_child(node);
                return parse_recover!($parser, $return);
            }
        }
    };
}

/// Eat all tokens as errors that do not satisfy the expected indentation.
#[macro_export]
macro_rules! parse_ensure_indentation {
    ($parser:expr, $return:expr) => {
        if !$parser.is_correct_indentation() {
            let mut node = $parser.empty_node($crate::concrete::Kind::Error(
                $crate::concrete::ErrorType::IncorrectIndentation,
            ));

            while !$parser.is_correct_indentation() && $parser.is(gdscript_lexer::TokenKind::End) {
                let err = $parser.eat_error($crate::concrete::ErrorType::IncorrectIndentation);
                node.span.1 = err.span.1;
            }

            $return.add_child(node);
        }
    };
}

pub mod class;

use crate::TokenIndex;
use codespan::FileMap;
use gdscript_lexer::token::{
    Location, Punctuation, Span, Token, TokenContent, TokenKind, TokenKind as TK,
};

/// The kind of a node describes what data it represents.
#[derive(Debug, Clone, Copy, Eq, PartialEq)]
#[allow(missing_docs)]
pub enum Kind {
    /// Unexpected input.
    Error(ErrorType),
    /// Lexical error encountered.
    LexicalError,

    End,

    Identifier,

    String,

    Newline,
    Semicolon,
    Period,

    ClassItemDocComment,
    DocComment,

    ClassItemClassName,

    Comment,

    File,

    KwTool,

    KwExtends,
    ExtendsPath,

    FuncAttribute,
    KwStatic,
    KwRemote,
    KwSync,
    KwMaster,
    KwPuppet,
    KwSlave,
    KwRemoteSync,
    KwMasterSync,
    KwPuppetSync,

    Function,
    KwFunc,
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
#[allow(missing_docs)]
pub enum ErrorType {
    UnexpectedToken,
    IncorrectIndentation,
}

/// The "weakly typed" building-block of a concrete-syntax-tree.
#[derive(Debug, Clone)]
pub struct Node {
    /// The kind of node.
    pub kind: Kind,
    /// The span of the node an all it's child-nodes.
    pub span: (TokenIndex, TokenIndex),
    /// List of child nodes associated with this node.
    pub children: Vec<Node>,
}

impl Node {
    /// Check if the current node is an error.
    #[inline]
    pub fn is_error(&self) -> bool {
        if let Kind::Error(_) = self.kind {
            true
        } else {
            false
        }
    }

    /// Adds a node as a child node and adjusts the token-span.
    pub fn add_child(&mut self, node: Node) {
        self.span.1 = node.span.1;
        self.children.push(node);
    }
}

/// Shorthand for parsing a token-list into a GDScript-file.
///
/// See the [`File`] type.
///
/// [`File`]: ./struct.File.html
pub fn parse_file<Src: AsRef<str>>(file_map: &FileMap<Src>, tokens: &[Token]) -> Node {
    let mut parser = ParseContext::new(file_map, tokens);

    parser.parse::<File>()
}

/// Result type of a parser.
pub type ParseResult = Result<Node, Node>;

/// Trait used to parse a term.
trait Parse {
    /// Returns true if the given TokenContent can be the beginning of the term.
    fn accept(parser: &ParseContext<'_, impl AsRef<str>>, tok: TokenContent) -> bool;

    /// Parse tokens into the term.
    fn parse(parser: &mut ParseContext<'_, impl AsRef<str>>) -> ParseResult;
}

pub(crate) struct ParseContext<'a, Src: AsRef<str>> {
    _file_map: &'a FileMap<Src>,
    toks: &'a [Token],

    tok_idx: TokenIndex,

    follow: Vec<fn(&Self, TokenContent) -> bool>,
    indents: Vec<usize>,
}

impl<'a, Src: AsRef<str>> ParseContext<'a, Src> {
    fn new(file_map: &'a FileMap<Src>, tokens: &'a [Token]) -> ParseContext<'a, Src> {
        ParseContext {
            _file_map: file_map,
            toks: tokens,
            tok_idx: 0,
            follow: vec![],
            indents: vec![0],
        }
    }

    fn parse<T: Parse>(&mut self) -> Node {
        self.push_follow(T::accept);
        let res = T::parse(self);
        self.pop_follow();
        parse_unwrap!(res)
    }

    fn accept<T: Parse>(&self, tok: TokenContent) -> bool {
        T::accept(self, tok)
    }

    fn is(&self, kind: TokenKind) -> bool {
        let tok = self.peek();

        tok.content.is_kind(kind)
    }

    fn is_eof(&self) -> bool {
        let tok = self.peek();

        tok.content.is_kind(TokenKind::End) || tok.content.is_kind(TokenKind::Error)
    }

    fn is_follow(&self) -> bool {
        let tok = self.peek();

        let len = self.follow.len();
        for i in 0..len {
            let idx = (len - 1) - i;
            if (self.follow[idx])(self, tok.content) {
                return true;
            }
        }
        false
    }

    fn peek(&self) -> Token {
        if let Some(tok) = self.toks.get(self.tok_idx) {
            *tok
        } else {
            Token {
                span: Span::new(Location::none(), Location::none()),
                content: TokenContent::End,
            }
        }
    }

    fn advance(&mut self) {
        // TODO do stuff like reset flags and all that?
        self.tok_idx += 1;
    }

    fn _lookahead(&self) -> Token {
        if let Some(tok) = self.toks.get(self.tok_idx + 1) {
            *tok
        } else {
            Token {
                span: Span::new(Location::none(), Location::none()),
                content: TokenContent::End,
            }
        }
    }

    fn is_correct_indentation(&self) -> bool {
        let aimed_indent = self.indent();

        let tok = self.peek();

        let indent = match self._file_map.location(tok.span.start()) {
            Ok((_line, col)) => col.to_usize(),
            Err(_) => 0,
        };

        indent == aimed_indent
    }

    fn indent(&self) -> usize {
        self.indents.last().cloned().unwrap_or(0)
    }

    fn _push_indent(&mut self, indent: usize) {
        self.indents.push(indent);
    }

    fn _pop_indent(&mut self) -> usize {
        self.indents.pop().unwrap_or(0)
    }

    fn push_follow(&mut self, f: fn(&Self, TokenContent) -> bool) {
        self.follow.push(f);
    }

    fn pop_follow(&mut self) {
        self.follow.pop();
    }

    fn empty_node(&self, kind: Kind) -> Node {
        Node {
            kind,
            span: (self.tok_idx, self.tok_idx),
            children: vec![],
        }
    }

    fn eat_token(&mut self, kind: Kind) -> Node {
        let node = Node {
            kind,
            span: (self.tok_idx, self.tok_idx),
            children: vec![],
        };

        self.advance();

        node
    }

    /// Emit an error node without consuming the token
    fn emit_error(&self, error: ErrorType) -> Node {
        let node = Node {
            kind: Kind::Error(error),
            span: (self.tok_idx, self.tok_idx),
            children: vec![],
        };

        node
    }

    /// Emit an error node and consume the token
    fn eat_error(&mut self, error: ErrorType) -> Node {
        let node = self.emit_error(error);
        self.advance();
        node
    }

    fn expect_tok(&mut self, token_content: TokenContent, kind: Kind) -> Option<Node> {
        let tok = self.peek();

        if tok.content != token_content {
            return None;
        }

        let node = Node {
            kind,
            span: (self.tok_idx, self.tok_idx),
            children: vec![],
        };

        self.advance();

        Some(node)
    }

    fn expect(&mut self, tok_kind: TokenKind, kind: Kind) -> Option<Node> {
        if !self.is(tok_kind) {
            return None;
        }

        let node = Node {
            kind,
            span: (self.tok_idx, self.tok_idx),
            children: vec![],
        };

        self.advance();

        Some(node)
    }
}

/// A GDScript file representing a class.
pub struct File;

impl Parse for File {
    fn accept(parser: &ParseContext<impl AsRef<str>>, tok: TokenContent) -> bool {
        if tok == TokenContent::End {
            return true;
        }

        if tok.is_kind(TokenKind::Error) {
            return true;
        }

        if parser.accept::<LineTerminator>(tok) {
            return true;
        }

        if parser.accept::<class::Item>(tok) {
            return true;
        }
        false
    }

    fn parse(parser: &mut ParseContext<impl AsRef<str>>) -> ParseResult {
        let mut node = parser.empty_node(Kind::File);

        loop {
            let tok = parser.peek();

            if tok.content.is_kind(TK::End) {
                break;
            }

            if tok.content.is_kind(TK::Error) {
                return Err(node);
            }

            // don't deal with indentation if it's an empty line
            if !parser.accept::<LineTerminator>(tok.content) {
                parse_ensure_indentation!(parser, node);
            }

            if parser.accept::<class::Item>(tok.content) {
                let item = parse_unwrap!(class::Item::parse(parser));
                node.add_child(item);
            } else if parser.accept::<LineTerminator>(tok.content) {
                let newline = parse_unwrap!(LineTerminator::parse(parser));
                node.add_child(newline);
            } else {
                match tok.content.get_kind() {
                    _ => {
                        // invalid token
                        node.add_child(parser.eat_error(ErrorType::UnexpectedToken))
                    }
                }
            }
        }

        Ok(node)
    }
}

pub(crate) fn child_span(children: &[Node]) -> (TokenIndex, TokenIndex) {
    let first = if let Some(node) = children.first() {
        node.span.0
    } else {
        0
    };

    let last = if let Some(node) = children.last() {
        node.span.1
    } else {
        first
    };

    (first, last)
}

pub(crate) struct LineTerminator;

impl Parse for LineTerminator {
    fn accept(_parser: &ParseContext<impl AsRef<str>>, tok: TokenContent) -> bool {
        if tok.is_kind(TK::Newline) {
            true
        } else if tok.is_kind(TK::End) {
            true
        } else if tok.is_kind(TK::Comment) {
            true
        } else if tok == TokenContent::Punctuation(Punctuation::Semicolon) {
            true
        } else {
            false
        }
    }

    fn parse(parser: &mut ParseContext<impl AsRef<str>>) -> ParseResult {
        let tok = parser.peek();

        let node = if tok.content.is_kind(TK::Newline) {
            parser.eat_token(Kind::Newline)
        } else if tok.content.is_kind(TK::Comment) {
            let mut comment = parser.eat_token(Kind::Comment);

            // WARNING this recurses here, but since a comment is terminated
            // by a newline (or end of stream) it is guaranteed that this
            // recursion terminates.
            let actual_terminator = parse_unwrap!(Self::parse(parser));

            comment.add_child(actual_terminator);

            comment
        } else if tok.content.is_kind(TK::End) {
            parser.eat_token(Kind::End)
        } else if tok.content == TokenContent::Punctuation(Punctuation::Semicolon) {
            parser.eat_token(Kind::Semicolon)
        } else {
            return Err(parser.emit_error(ErrorType::UnexpectedToken));
        };

        Ok(node)
    }
}
