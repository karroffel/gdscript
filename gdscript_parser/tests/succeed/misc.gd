tool
extends Node # this is an example script!

class_name ExampleScript "res://image.png" # Associate name and icon.

# this time without an icon-path
class_name ExampleScript

"""
This node does something, but not now. It will in future.
"""

# comments are supported as well.