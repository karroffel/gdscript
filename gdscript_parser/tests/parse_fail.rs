/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

extern crate gdscript_lexer;
extern crate gdscript_parser;

mod common;

use gdscript_lexer::lexer::tokenize;
use gdscript_parser::concrete::parse_file;

#[test]
fn parse_fail() -> Result<(), Box<dyn std::error::Error>> {
    for file in common::file_iter("./tests/fail") {
        let toks = tokenize(&file);

        let tree = parse_file(&file, &toks);

        let has_errors = common::has_errors(&tree);

        assert_eq!(has_errors, true, "File {:?}", file.name());
    }

    Ok(())
}
