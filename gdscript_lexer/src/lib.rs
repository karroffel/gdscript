/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#![warn(missing_docs)]

//! Lexing library for the GDScript programming language.

/// Definition of the "atoms" of this lexer: tokens
pub mod token;

/// Implementation of the tokenizer
pub mod lexer;

pub use token::{Token, TokenKind};

#[cfg(test)]
mod tests {
    use super::token::TokenContent::*;
    use crate::lexer::tokenize;
    use crate::token::Token;
    use crate::token::{Sign, StringType, TokenContent};
    use codespan::{FileMap, FileName};

    struct TokTest {
        file_map: FileMap<&'static str>,
    }

    impl TokTest {
        pub fn new(src: &'static str) -> Self {
            let file_name = FileName::virtual_("test");
            let file_map = FileMap::new(file_name, src);

            TokTest { file_map }
        }

        pub fn tokenize(&self) -> Vec<Token> {
            tokenize(&self.file_map)
        }

        pub fn identifier(&self, token: Token) -> &str {
            if let TokenContent::Identifier(span) = token.content {
                let content = self.file_map.src_slice(span).unwrap();

                content
            } else {
                "error"
            }
        }

        pub fn number(&self, token: Token) -> (&str, &str, Sign, &str) {
            if let TokenContent::Number(num) = token.content {
                use crate::token::Number as N;

                if let N::Decimal {
                    integer,
                    fraction,
                    exponent,
                } = num
                {
                    let integer_slice = self.file_map.src_slice(integer).unwrap();
                    let fraction_slice = self.file_map.src_slice(fraction).unwrap();
                    let exponent_slice = self.file_map.src_slice(exponent.1).unwrap();

                    (integer_slice, fraction_slice, exponent.0, exponent_slice)
                } else {
                    ("error", "error", Sign::Negative, "error")
                }
            } else {
                ("error", "error", Sign::Negative, "error")
            }
        }

        pub fn num_hex(&self, token: Token) -> &str {
            if let TokenContent::Number(num) = token.content {
                use crate::token::Number as N;

                if let N::Hexadecimal {
                    content: content_span,
                } = num
                {
                    let slice = self.file_map.src_slice(content_span).unwrap();

                    slice
                } else {
                    "error"
                }
            } else {
                "error"
            }
        }

        pub fn string(&self, token: Token) -> (StringType, &str) {
            if let TokenContent::String(string) = token.content {
                let slice = self.file_map.src_slice(string.content).unwrap();

                (string.ty, slice)
            } else {
                (StringType::SingleQuote, "error")
            }
        }

        pub fn node_path(&self, token: Token) -> (StringType, &str) {
            if let TokenContent::NodePath(string) = token.content {
                let slice = self.file_map.src_slice(string.content).unwrap();

                (string.ty, slice)
            } else {
                (StringType::SingleQuote, "error")
            }
        }

        pub fn comment(&self, token: Token) -> &str {
            if let TokenContent::Comment(comment) = token.content {
                let slice = self.file_map.src_slice(comment).unwrap();
                slice
            } else {
                "error"
            }
        }
    }

    #[test]
    fn empty_input() {
        let test = TokTest::new("");
        let toks = test.tokenize();

        assert_eq!(toks.len(), 1);
        assert_eq!(toks[0].content, End);
    }

    #[test]
    fn newline_ignore() {
        use crate::token::Error::UnexpectedCharacter;
        {
            let test = TokTest::new("\\\n");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 1);
            assert_eq!(toks[0].content, TokenContent::End);
        }

        {
            let test = TokTest::new("\\\r\n");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 1);
            assert_eq!(toks[0].content, TokenContent::End);
        }

        {
            let test = TokTest::new("\\\r");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 1);
            assert_eq!(
                toks[0].content,
                TokenContent::Error(UnexpectedCharacter {
                    expected: "newline after \'\\\'",
                    got: '\r',
                })
            );
        }

        {
            let test = TokTest::new("Hello\\\n    World");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 3);
            assert_eq!(test.identifier(toks[0]), "Hello");
            assert_eq!(test.identifier(toks[1]), "World");
            assert_eq!(toks[2].content, TokenContent::End);
        }

        {
            let test = TokTest::new("Hello\\\r\n    World");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 3);
            assert_eq!(test.identifier(toks[0]), "Hello");
            assert_eq!(test.identifier(toks[1]), "World");
            assert_eq!(toks[2].content, TokenContent::End);
        }

        {
            use crate::token::Error as E;
            let test = TokTest::new("\\    World");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 1);
            assert_eq!(
                toks[0].content,
                TokenContent::Error(E::UnexpectedCharacter {
                    expected: "newline after '\\'",
                    got: ' ',
                })
            );
        }
    }

    #[test]
    fn newline() {
        {
            let test = TokTest::new("\n");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 2);
            assert_eq!(toks[0].content, TokenContent::Newline);
            assert_eq!(toks[1].content, TokenContent::End);
        }

        {
            let test = TokTest::new("\r\n");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 2);
            assert_eq!(toks[0].content, TokenContent::Newline);
            assert_eq!(toks[1].content, TokenContent::End);
        }

        {
            let test = TokTest::new("\r");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 1);
            assert_eq!(toks[0].content, TokenContent::End);
        }
    }

    #[test]
    fn keyword_operator_identifier() {
        use crate::token::Keyword as KeyW;
        use crate::token::Operator as Op;
        // identifier
        {
            let test = TokTest::new("hello");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 2);
            assert_eq!(test.identifier(toks[0]), "hello");
            assert_eq!(toks[1].content, End);
        }

        // keyword identifier
        {
            let test = TokTest::new("extends Node");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 3);
            assert_eq!(toks[0].content, Keyword(KeyW::Extends));
            assert_eq!(test.identifier(toks[1]), "Node");
            assert_eq!(toks[2].content, End);
        }

        // keyword keyword identifier
        {
            let test = TokTest::new("export var x");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 4);
            assert_eq!(toks[0].content, Keyword(KeyW::Export));
            assert_eq!(toks[1].content, Keyword(KeyW::Var));
            assert_eq!(test.identifier(toks[2]), "x");
            assert_eq!(toks[3].content, End);
        }

        // operator
        {
            let test = TokTest::new("a and b or c");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 6);
            assert_eq!(test.identifier(toks[0]), "a");
            assert_eq!(toks[1].content, Operator(Op::And));
            assert_eq!(test.identifier(toks[2]), "b");
            assert_eq!(toks[3].content, Operator(Op::Or));
            assert_eq!(test.identifier(toks[4]), "c");
            assert_eq!(toks[5].content, End);
        }
    }

    #[test]
    fn operators() {
        use crate::token::Operator as Op;

        {
            let test = TokTest::new("a && b || c");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 6);
            assert_eq!(test.identifier(toks[0]), "a");
            assert_eq!(toks[1].content, Operator(Op::And));
            assert_eq!(test.identifier(toks[2]), "b");
            assert_eq!(toks[3].content, Operator(Op::Or));
            assert_eq!(test.identifier(toks[4]), "c");
            assert_eq!(toks[5].content, End);
        }

        {
            let test = TokTest::new("a * a + b * b == c * c");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 12);
            assert_eq!(test.identifier(toks[0]), "a");
            assert_eq!(toks[1].content, Operator(Op::Mul));
            assert_eq!(test.identifier(toks[2]), "a");
            assert_eq!(toks[3].content, Operator(Op::Add));
            assert_eq!(test.identifier(toks[4]), "b");
            assert_eq!(toks[5].content, Operator(Op::Mul));
            assert_eq!(test.identifier(toks[6]), "b");
            assert_eq!(toks[7].content, Operator(Op::Equal));
            assert_eq!(test.identifier(toks[8]), "c");
            assert_eq!(toks[9].content, Operator(Op::Mul));
            assert_eq!(test.identifier(toks[10]), "c");
            assert_eq!(toks[11].content, End);
        }
    }

    #[test]
    fn punctuation() {
        use crate::token::Keyword as KeyW;
        use crate::token::Punctuation as Punc;
        use crate::token::Sign;

        {
            let test = TokTest::new("func test(a) -> void;");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 9);
            assert_eq!(toks[0].content, Keyword(KeyW::Func));
            assert_eq!(test.identifier(toks[1]), "test");
            assert_eq!(toks[2].content, Punctuation(Punc::ParenthesisOpen));
            assert_eq!(test.identifier(toks[3]), "a");
            assert_eq!(toks[4].content, Punctuation(Punc::ParenthesisClose));
            assert_eq!(toks[5].content, Punctuation(Punc::Arrow));
            assert_eq!(toks[6].content, Keyword(KeyW::Void));
            assert_eq!(toks[7].content, Punctuation(Punc::Semicolon));
            assert_eq!(toks[8].content, End);
        }

        {
            let test = TokTest::new("a.b");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 4);
            assert_eq!(test.identifier(toks[0]), "a");
            assert_eq!(toks[1].content, Punctuation(Punc::Period));
            assert_eq!(test.identifier(toks[2]), "b");
            assert_eq!(toks[3].content, End);
        }

        {
            let test = TokTest::new("1.2.b");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 4);
            assert_eq!(test.number(toks[0]), ("1", "2", Sign::Positive, ""));
            assert_eq!(toks[1].content, Punctuation(Punc::Period));
            assert_eq!(test.identifier(toks[2]), "b");
            assert_eq!(toks[3].content, End);
        }
    }

    #[test]
    fn string() {
        use crate::token::Error as ER;
        use crate::token::StringType as ST;

        // single quote

        {
            let test = TokTest::new("\'\'");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 2);
            assert_eq!(test.string(toks[0]), (ST::SingleQuote, ""));
            assert_eq!(toks[1].content, End);
        }

        {
            let test = TokTest::new("\'hello\'");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 2);
            assert_eq!(test.string(toks[0]), (ST::SingleQuote, "hello"));
            assert_eq!(toks[1].content, End);
        }

        {
            let test = TokTest::new("\'hello");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 1);
            assert_eq!(toks[0].content, Error(ER::UnclosedStringLiteral));
        }

        // double quote

        {
            let test = TokTest::new("\"\"");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 2);
            assert_eq!(test.string(toks[0]), (ST::DoubleQuote, ""));
            assert_eq!(toks[1].content, End);
        }

        {
            let test = TokTest::new("\"hello\"");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 2);
            assert_eq!(test.string(toks[0]), (ST::DoubleQuote, "hello"));
            assert_eq!(toks[1].content, End);
        }

        {
            let test = TokTest::new("\"hello");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 1);
            assert_eq!(toks[0].content, Error(ER::UnclosedStringLiteral));
        }

        // multi-line quote

        {
            let test = TokTest::new("\"\"\"\"\"\"");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 2);
            assert_eq!(test.string(toks[0]), (ST::MultiLine, ""));
            assert_eq!(toks[1].content, End);
        }

        {
            let test = TokTest::new("\"\"\"hello\"\"\"");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 2);
            assert_eq!(test.string(toks[0]), (ST::MultiLine, "hello"));
            assert_eq!(toks[1].content, End);
        }

        {
            let test = TokTest::new("\"\"\"hello\"\"");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 1);
            assert_eq!(toks[0].content, Error(ER::UnclosedStringLiteral));
        }

        // escape sequences

        {
            let test = TokTest::new("\"\\\"\"");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 2);
            assert_eq!(test.string(toks[0]), (ST::DoubleQuote, "\\\""));
            assert_eq!(toks[1].content, End);
        }

        {
            let test = TokTest::new("\"\\u048B\"");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 2);
            assert_eq!(test.string(toks[0]), (ST::DoubleQuote, "\\u048B"));
            assert_eq!(toks[1].content, End);
        }
    }

    #[test]
    fn node_path() {
        use crate::token::Error as ER;
        use crate::token::StringType as ST;

        // single quote

        {
            let test = TokTest::new("@\'\'");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 2);
            assert_eq!(test.node_path(toks[0]), (ST::SingleQuote, ""));
            assert_eq!(toks[1].content, End);
        }

        // double quote
        {
            let test = TokTest::new("@\"hello\"");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 2);
            assert_eq!(test.node_path(toks[0]), (ST::DoubleQuote, "hello"));
            assert_eq!(toks[1].content, End);
        }

        // no string at all
        {
            let test = TokTest::new("@");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 1);
            assert_eq!(toks[0].content, Error(ER::UnexpectedEnd));
        }

        // unclosed string
        {
            let test = TokTest::new("@\'");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 1);
            assert_eq!(toks[0].content, Error(ER::UnclosedStringLiteral));
        }

        // not a string
        {
            let test = TokTest::new("@a");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 1);
            assert_eq!(
                toks[0].content,
                Error(ER::UnexpectedCharacter {
                    got: 'a',
                    expected: "beginning quote of a string constant",
                })
            );
        }
    }

    #[test]
    fn comment() {
        {
            let test = TokTest::new("# Hi");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 2);
            assert_eq!(test.comment(toks[0]), " Hi");
            assert_eq!(toks[1].content, End);
        }

        {
            let test = TokTest::new("# Hi\n");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 3);
            assert_eq!(test.comment(toks[0]), " Hi");
            assert_eq!(toks[1].content, Newline);
            assert_eq!(toks[2].content, End);
        }

        {
            let test = TokTest::new("hi # Hi");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 3);
            assert_eq!(test.identifier(toks[0]), "hi");
            assert_eq!(test.comment(toks[1]), " Hi");
            assert_eq!(toks[2].content, End);
        }

        {
            let test = TokTest::new("hi # Hi\n");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 4);
            assert_eq!(test.identifier(toks[0]), "hi");
            assert_eq!(test.comment(toks[1]), " Hi");
            assert_eq!(toks[2].content, Newline);
            assert_eq!(toks[3].content, End);
        }
    }

    #[test]
    fn numbers() {
        use crate::token::Sign;

        // hex
        {
            let test = TokTest::new("0xDEADBEEF");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 2);
            assert_eq!(test.num_hex(toks[0]), "DEADBEEF");
            assert_eq!(toks[1].content, End);
        }
        {
            let test = TokTest::new("0xDEADBEEF ");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 2);
            assert_eq!(test.num_hex(toks[0]), "DEADBEEF");
            assert_eq!(toks[1].content, End);
        }

        // "int"
        {
            let test = TokTest::new("42");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 2);
            assert_eq!(test.number(toks[0]), ("42", "", Sign::Positive, ""));
            assert_eq!(toks[1].content, End);
        }
        {
            let test = TokTest::new("42 ");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 2);
            assert_eq!(test.number(toks[0]), ("42", "", Sign::Positive, ""));
            assert_eq!(toks[1].content, End);
        }

        // "float" without exponent
        {
            let test = TokTest::new("13.37");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 2);
            assert_eq!(test.number(toks[0]), ("13", "37", Sign::Positive, ""));
            assert_eq!(toks[1].content, End);
        }
        {
            let test = TokTest::new("13.37 ");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 2);
            assert_eq!(test.number(toks[0]), ("13", "37", Sign::Positive, ""));
            assert_eq!(toks[1].content, End);
        }

        // "float" with positive exponent
        {
            let test = TokTest::new("13.37e12");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 2);
            assert_eq!(test.number(toks[0]), ("13", "37", Sign::Positive, "12"));
            assert_eq!(toks[1].content, End);
        }
        {
            let test = TokTest::new("13.37e12 ");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 2);
            assert_eq!(test.number(toks[0]), ("13", "37", Sign::Positive, "12"));
            assert_eq!(toks[1].content, End);
        }
        {
            let test = TokTest::new("13.37e+12");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 2);
            assert_eq!(test.number(toks[0]), ("13", "37", Sign::Positive, "12"));
            assert_eq!(toks[1].content, End);
        }
        {
            let test = TokTest::new("13.37e+12 ");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 2);
            assert_eq!(test.number(toks[0]), ("13", "37", Sign::Positive, "12"));
            assert_eq!(toks[1].content, End);
        }

        // "float" with negative exponent
        {
            let test = TokTest::new("13.37e-12");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 2);
            assert_eq!(test.number(toks[0]), ("13", "37", Sign::Negative, "12"));
            assert_eq!(toks[1].content, End);
        }
        {
            let test = TokTest::new("13.37e-12 ");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 2);
            assert_eq!(test.number(toks[0]), ("13", "37", Sign::Negative, "12"));
            assert_eq!(toks[1].content, End);
        }

        // "int" with positive exp
        {
            let test = TokTest::new("13e12");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 2);
            assert_eq!(test.number(toks[0]), ("13", "", Sign::Positive, "12"));
            assert_eq!(toks[1].content, End);
        }
        // "int" with negative exp
        {
            let test = TokTest::new("13e12 ");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 2);
            assert_eq!(test.number(toks[0]), ("13", "", Sign::Positive, "12"));
            assert_eq!(toks[1].content, End);
        }

        // "int" with dot with positive exp
        {
            let test = TokTest::new("13.e12");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 2);
            assert_eq!(test.number(toks[0]), ("13", "", Sign::Positive, "12"));
            assert_eq!(toks[1].content, End);
        }
        // "int" with dot with negative exp
        {
            let test = TokTest::new("13.e-12");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 2);
            assert_eq!(test.number(toks[0]), ("13", "", Sign::Negative, "12"));
            assert_eq!(toks[1].content, End);
        }

        // "float" without integer part
        {
            let test = TokTest::new(".5");
            let toks = test.tokenize();

            assert_eq!(toks.len(), 2);
            assert_eq!(test.number(toks[0]), ("", "5", Sign::Positive, ""));
            assert_eq!(toks[1].content, End);
        }
    }
}
